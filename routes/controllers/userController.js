const userController = {};

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const speakeasy = require('speakeasy');
const qrcode = require('qrcode');

const db = require('../db/db');
const config = require('../config/config');
const tokenVerification = require('../authentication/tokenVerification');

userController.register = function (req) {
    if (isValidUserRequest(req)) {
        const hashedPassword = hashPassword(req.body.password);
        db.addUser(req.body.email, hashedPassword);
        const user = { email: req.body.email };
        user.token = jwt.sign(user, config.jwt.secret);
        delete user.password;
        return(user);
    }
    return null
};

userController.login = function (req) {
    if (isValidUserRequest(req)) {
        let user = db.getCleanedUser(req.body.email);
        if (typeof user !== 'undefined') {
            if (bcrypt.compareSync(req.body.password, user.password)) {
                delete user.password;
                const token = jwt.sign(user, config.jwt.secret);
                const newUser = { ...user, token };
                return(newUser);
            }
        }
    }
    return null
};

function isValidUserRequest(req) {
return req.body && req.body.email && req.body.email.length > 0 && req.body.password && req.body.password.length > 0;
}

function hashPassword(password) {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt);
  }
  
module.exports = userController;
