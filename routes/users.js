var express = require('express');
var router = express.Router();

const userController = require('./controllers/userController');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/', function(req, res, next) {
  const user = userController.register(req)
  if (user) {
    res.send(user);
  }else {
    res.statusCode(404).send('User not found');
  }
});

router.post('/login', function(req, res, next) {
  const user = userController.login(req)
  if (user) {
    res.send(user);
  }else {
    res.statusCode(404).send('Invalid login. Please try again.');
  }
});


module.exports = router;
